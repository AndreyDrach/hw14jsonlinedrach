const tabsElements = document.querySelector(".tabs");
const contentCollection = document.querySelector(".tabs-content");
const dataTab = contentCollection.querySelectorAll("[data-tab]");
const iconBackground = document.querySelector(".brightness");
const body = document.querySelector("body");

tabsElements.addEventListener("click", (e) => {
  tabsElements.childNodes.forEach((elem) => (elem.className = "tabs-title"));
  e.target.classList.add("active");

  dataTab.forEach((elem) => {
    elem.className = "tabs-content-display-none";
    if (elem.dataset.tab == e.target.dataset.tab) {
      elem.classList.remove("tabs-content-display-none");
    }
  });
});

body.className = localStorage.getItem("className");
iconBackground.addEventListener("click", () => {
  if (body.classList.contains("dark-background")) {
    body.classList.remove("dark-background");
  } else {
    body.classList.add("dark-background");
  }
  localStorage.setItem("className", `${body.className}`);
});
